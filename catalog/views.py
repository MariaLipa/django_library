from django.http import HttpResponse, HttpRequest


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse(
        "<p>Hello World!</p><p>Django є одним з найбільших framework на Python</p><hr>"
    )
